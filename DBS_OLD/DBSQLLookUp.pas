unit DBSQLLookUp;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Dialogs, Windows, DBGridEh, Forms, WinApi.Messages,
  DBCtrlsEh, Data.DbxSqlite, Data.SqlExpr, Data.FMTBcd, DataDriverEh, DBXDataDriverEh, ToolCtrlsEh, EhLibVCL, DB, ADODB, Variants, GridsEh,
  IniFiles, MemTableDataEh, MemTableEh, ExtCtrls, Generics.Collections, JSON, FMAParamTransform, FMACrudAgent, FMACrudAgentInterface;

type

  TDBSQLLookUp = class(TDBEditEh)
  private
    FCrudAgent: IFMACrudAgent;
    FKeyValue: Variant;
    FKeyFieldName: string;
    FDisplayValue: string;
    FDisplayFieldName: string;
    FPairList : TList<TPair<Variant, string>>;
    FLookUpWindow: TPopupListboxEh;
    FEditing: Boolean;
    FOnKeyValueChange: TNotifyEvent;
    FRowCount: integer;
    FSearchFromStart: boolean;
    procedure DoBtnClick(Sender: TObject; var Handled: Boolean);
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;
    procedure ListMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FillItems;
  protected
    function GetVariantValue: Variant; override;
    procedure Loaded; override;
    procedure KeyPress(var key: Char); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Change; override;
    procedure DataChanged; override;
    procedure SetKeyValue(value: Variant);
    procedure SetDisplayValue(value: string);
    procedure DoEnter; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure UpdateData; override;
    procedure SelectNextValue(IsPrior: Boolean);
    procedure DropDown;
    procedure SetFirstValue;
    procedure CloseUp(Accept: Boolean); override;
    destructor Destroy; override;
  published
    property CrudAgent: IFMACrudAgent read FCrudAgent write FCrudAgent;
    property KeyValue: Variant read FKeyValue write SetKeyValue;
    property DisplayValue: string read FDisplayValue write SetDisplayValue;
    property KeyFieldName: string read FKeyFieldName write FKeyFieldName;
    property DisplayFieldName: string read FDisplayFieldName write FDisplayFieldName;
    property RowCount: integer read FRowCount write FRowCount;
    property SearchFromStart: boolean read FSearchFromStart write FSearchFromStart;
    property OnKeyValueChange: TNotifyEvent read FOnKeyValueChange write FOnKeyValueChange;
  end;


function NullValue(s: string): Variant;

implementation

procedure TDBSQLLookUp.SetKeyValue(value: Variant);
begin
  FKeyValue := value;
  if Assigned(self.DataSource) then
    if Assigned(self.DataSource.DataSet) then
      if self.DataSource.DataSet.State in [dsEdit, dsInsert] then
        self.DataSource.DataSet.FieldByName(self.DataField).Value := FKeyValue;

  if Assigned(FOnKeyValueChange) then FOnKeyValueChange(self);

  DataChanged;

end;


procedure TDBSQLLookUp.SelectNextValue(IsPrior: Boolean);
var
  OldItemIndex: Integer;
begin

  OldItemIndex := FLookUpWindow.ItemIndex;
  if IsPrior then
  begin
    if FLookUpWindow.ItemIndex > 0 then
      FLookUpWindow.ItemIndex := FLookUpWindow.ItemIndex - 1;
  end
  else if FLookUpWindow.ItemIndex < FLookUpWindow.Items.Count - 1 then
    FLookUpWindow.ItemIndex := FLookUpWindow.ItemIndex + 1;

  if OldItemIndex <> FLookUpWindow.ItemIndex then
  begin
    SelectAll;
  end;
end;


constructor TDBSQLLookUp.Create(AOwner: TComponent);
var btn: TEditButtonEh;
begin
  inherited Create(AOwner);
  btn := self.EditButtons.Add;
  btn.Style := ebsDropDownEh;
  btn.OnClick := DoBtnClick;
  btn.Visible := true;
  if (csDesigning in self.ComponentState) then exit;
  FLookUpWindow := TPopupListboxEh.Create(self);
  FLookUpWindow.Visible := false;
  FLookUpWindow.Parent := self;
  EditButtons[0].OnClick := DoBtnClick;
  FLookUpWindow.OnMouseUp := ListMouseUp;
  FKeyValue := null;
  FPairList := TList<TPair<Variant, string>>.Create;
end;


destructor TDBSQLLookUp.Destroy;
begin
  inherited;
end;


procedure TDBSQLLookUp.DoBtnClick(Sender: TObject; var Handled: Boolean);
var errormessage: string; result_state: boolean;
begin

  FCRUDAgent.ParamTransform.ResetContext;

  {if FSearchFromStart then
    FCRUDAgent.ParamTransform.AddPatternContext('/*wherefilter*/', 'lower('+self.FDisplayFieldName+') like lower('''+trim(Text)+'%'+''')')
  else
    FCRUDAgent.ParamTransform.AddPatternContext('/*wherefilter*/', 'lower('+self.FDisplayFieldName+') like lower('''+'%'+trim(Text)+'%'+''')');
  }

  FCRUDAgent.ParamTransform.AddPatternContext('/*limit*/', '100');

  FCrudAgent.FillDictionary(
    FPairList,
    FCRUDAgent.ParamTransform.Context,
    FKeyFieldName,
    FDisplayFieldName,
    result_state,
    errormessage);
  FillItems;
  DropDown;
end;

procedure TDBSQLLookUp.DoEnter;
begin
  inherited;
  SelectAll;
end;

procedure TDBSQLLookUp.Loaded;
begin
  if (csDesigning in self.ComponentState) then exit;
  SetControlReadOnly(false);
  self.EditButtons[0].OnClick := DoBtnClick;
  self.FLookUpWindow.OnMouseUp := self.ListMouseUp;
  self.FLookUpWindow.ItemHeight := 20;
  self.Alignment := taLeftJustify;
end;

procedure TDBSQLLookUp.KeyPress(var key: Char);
var errormessage: string;
begin

  if (csDesigning in self.ComponentState) then exit;

  if CharInSetEh(Key, [#13, #27]) then
  begin
    CloseUp(Key=#13);
    Key := #0;
  end;

  if Key = #0 then exit;

end;

procedure TDBSQLLookUp.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if ((Key = VK_UP) or (Key = VK_DOWN)) then
  begin
    SelectNextValue(Key = VK_UP);
  end;
end;

procedure TDBSQLLookUp.DropDown;
var P: TPoint;
begin

  if self.ReadOnly then exit;

  if Assigned(self.DataSource) then
  begin
    if not self.DataSource.DataSet.CanModify then exit;
    if not self.DataSource.DataSet.FieldByName(self.DataField).CanModify then exit;
  end;

  self.WantReturns := true;

  FLookUpWindow.Width := Width;

  if FRowCount>0 then
  FLookUpWindow.RowCount := self.FRowCount;

  P := self.Parent.ClientToScreen(Point(self.Left,self.Top+self.Height+2));
  SetWindowPos(FLookUpWindow.Handle, HWND_TOP, P.X, P.Y, 0, 0, SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
  FLookUpWindow.Visible := True;
  FLookUpWindow.SizeGripAlwaysShow := true;
end;

procedure TDBSQLLookUp.FillItems;
var i: integer; pair: TPair<Variant, string>;
begin
  FLookUpWindow.ItemIndex := -1;
  FLookUpWindow.Items.Clear;
  for i := 0 to FPairList.Count-1 do
  begin
    pair := FPairList.Items[i];
    FLookUpWindow.Items.Add(pair.Value);
  end;
end;

procedure TDBSQLLookUp.SetDisplayValue(value: string);
begin
 FEditing := true;
 FDisplayValue := value;
 Text := value;
 FEditing := false;
end;

procedure TDBSQLLookUp.SetFirstValue;
var errormessage: string; result_state: boolean;
begin

  FCrudAgent.FillDictionary(
    FPairList,
    TJSONObject.Create,
    FKeyFieldName,
    FDisplayFieldName,
    result_state,
    errormessage);
  FillItems;

  if FPairList.Count>0 then
  begin
    if FLookUpWindow.ItemIndex<FLookUpWindow.Items.Count then
    DisplayValue := FLookUpWindow.Items[FLookUpWindow.ItemIndex];
    KeyValue := FPairList[FLookUpWindow.ItemIndex].Key;
  end;

end;

procedure TDBSQLLookUp.CloseUp(Accept:boolean);
var errormessage: string; v: TPair<Variant, string>;
begin

  if not FLookUpWindow.Visible then exit;

  FLookUpWindow.Visible := false;
  if FLookUpWindow.Items.Count = 0 then exit;
  if FPairList.Count = 0 then exit;

  if Accept then
  begin
    if FLookUpWindow.ItemIndex<FPairList.Count then
    begin
      v := FPairList[FLookUpWindow.ItemIndex];
      KeyValue := v.Key;
      DisplayValue := v.Value;
    end;
  end;
end;

procedure TDBSQLLookUp.WMKillFocus(var Message: TWMKillFocus);
begin
  if FLookUpWindow.Visible and not (Message.FocusedWnd = FLookUpWindow.Handle) then
  begin
    CloseUp(false);
  end;
  FEditing := true;
  Text := FDisplayValue;
  SelectAll;
  UpdateData;
  inherited;
  FEditing := false;
end;


procedure TDBSQLLookUp.ListMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
  begin
      CloseUp(true);
  end;
end;

function TDBSQLLookUp.GetVariantValue: Variant;
begin
  result := self.FKeyValue;
end;

procedure TDBSQLLookUp.Change;
var errormessage: string; result_state: boolean;
begin

  if FEditing then exit;

  if self.Text = '' then
  begin
    self.KeyValue := null;
    self.FDisplayValue := '';
    exit;
  end;

  if not self.Focused then exit;

  if not Assigned(FCrudAgent) then exit;

  FEditing := true;

  try
    FCRUDAgent.ParamTransform.ResetContext;

    if FSearchFromStart then
      FCRUDAgent.ParamTransform.AddPatternContext('/*wherefilter*/', 'lower('+self.FDisplayFieldName+') like lower('''+trim(Text)+'%'+''')')
    else
      FCRUDAgent.ParamTransform.AddPatternContext('/*wherefilter*/', 'lower('+self.FDisplayFieldName+') like lower('''+'%'+trim(Text)+'%'+''')');

    FCRUDAgent.ParamTransform.AddPatternContext('/*limit*/', '100');

    FCrudAgent.FillDictionary(
      FPairList,
      FCRUDAgent.ParamTransform.Context,
      FKeyFieldName,
      FDisplayFieldName,
      result_state,
      errormessage);
    FillItems;
    FLookUpWindow.Refresh;
    DropDown;
  finally
    FEditing := false;
  end;

end;


procedure TDBSQLLookUp.UpdateData;
var handled: boolean;
begin
  if not Assigned(FCrudAgent) then exit;
  if Assigned(DataSource) then
    if Assigned(DataSource.DataSet) then
      DataSource.DataSet.FieldByName(self.DataField).Value := self.FKeyValue;

  if Assigned(OnUpdateData) then
    OnUpdateData(self, handled);
end;


procedure TDBSQLLookUp.DataChanged;
var field_value: Variant; field_type: TFieldType;
errormessage: string; result_state: boolean;
begin
  if not Assigned(FCrudAgent) then exit;
  if FEditing then exit;

  FEditing := true;

  try
    field_value := null;

    if Assigned(self.DataSource) and (Assigned(self.DataSource.DataSet)) then
    begin
      field_value := self.DataSource.DataSet.FieldByName(DataField).Value;
      field_type := self.DataSource.DataSet.FieldByName(DataField).DataType;

      FCRUDAgent.ParamTransform.ResetContext;
      FCRUDAgent.ParamTransform.AddPatternContext('/*wherefilter*/', FKeyFieldName+' = :'+FKeyFieldName);
      FCRUDAgent.ParamTransform.AddPatternContext('/*limit*/', '100');
      FCRUDAgent.ParamTransform.AddParameterContext(FKeyFieldName, field_type ,field_value);
      FCRUDAgent.AvoidEvents := true;

      FCrudAgent.FillDictionary(
        FPairList,
        FCRUDAgent.ParamTransform.Context,
        FKeyFieldName,
        FDisplayFieldName,
        result_state,
        errormessage);
      FillItems;

      FCRUDAgent.AvoidEvents := false;

      if FPairList.Count>0 then
      begin
        KeyValue := NullValue(FPairList[0].Key);
        DisplayValue := FPairList[0].Value;
      end else
      begin
        KeyValue := null;
        DisplayValue := '';
      end;
    end;
  finally
    FEditing := false;
  end;
end;


function NullValue(s: string): Variant;
begin
  if s = '' then result := null
  else result := s;
end;


end.
