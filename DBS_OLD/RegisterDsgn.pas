unit RegisterDsgn;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Dialogs, Windows, DBGridEh, Forms, WinApi.Messages,
  DBCtrlsEh, Data.DbxSqlite, Data.SqlExpr, Data.FMTBcd, DataDriverEh, DBXDataDriverEh, ToolCtrlsEh, EhLibVCL, DB, ADODB, Variants, GridsEh,
{$IFDEF DESIGNTIME}
  DesignEditors, DesignIntf,
{$ENDIF}
  DBSQLLookUp;

procedure Register;

implementation


procedure Register;
begin
  RegisterComponents('Samples', [TDBSQLLookUp]);
end;


end.
