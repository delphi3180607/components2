unit FMAVertPages;

interface

uses
  System.SysUtils, WinAPI.Windows, System.Classes, Vcl.Controls, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Forms, Vcl.Buttons, Vcl.ComCtrls, Vcl.Graphics, PngBitBtn;

type

  TFMAVertPages = class(TPanel)
  private
    FForms: TList;
    FButtons: TList;
    FVertPanel: TPanel;
    FWorkPlace: TPanel;
    FOnAddForm: TNotifyEvent;
    FOnChangeActiveForm: TNotifyEvent;
    FShowButtons: boolean;
    FActiveForm: TForm;
    function PageIndex(form: TForm): integer;
    procedure PlaceForm(form: TForm);
    procedure PlaceButton(button_caption: string);
    procedure PushButton(button: TPngBitBtn);
    procedure OnButtonClick(Sender: TObject);
    procedure SetShowButtons(Value: boolean);
  public
    constructor Create(AOwner: TComponent); override;
    procedure AddForm(form: TForm);
    procedure RemoveForm(form: TForm);
    procedure RemoveForms;
    procedure RemoveActiveForm;
    procedure ActivatePage(index: integer);
    property ActiveForm : TForm read FActiveForm;
  published
    property ShowButtons: boolean read FShowButtons write SetShowButtons;
    property OnAddForm: TNotifyEvent read FOnAddForm write FOnAddForm;
    property OnChangeActiveForm: TNotifyEvent read FOnChangeActiveForm write FOnChangeActiveForm;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Project Controls', [TFMAVertPages]);
end;

{ TFMAVertPages }

constructor TFMAVertPages.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FForms := TList.Create;
  FButtons := TList.Create;
  FVertPanel := TPanel.Create(self);
  FWorkPlace := TPanel.Create(self);

  with FWorkPlace do
  begin
    Align := alClient;
    BorderStyle := bsNone;
    BevelOuter := bvNone;
    BevelInner := bvNone;
    BevelKind := bkNone;
    Height := 30;
    Parent := self;
  end;

  with FVertPanel do
  begin
    Align := alLeft;
    BorderStyle := bsNone;
    BevelOuter := bvNone;
    BevelInner := bvNone;
    BevelKind := bkNone;
    AlignWithMargins := true;
    Width := 150;
    Parent := self;
    Visible := FShowButtons;
    Margins.Right := 20;
  end;
end;

procedure TFMAVertPages.OnButtonClick(Sender: TObject);
var index: integer;
begin
  index := FButtons.IndexOf(Sender);
  if index<0 then exit;
  ActivatePage(index);
end;

procedure TFMAVertPages.ActivatePage(index: integer);
begin
 if FActiveForm = FForms[index] then exit;
 PlaceForm(FForms[index]);
 PushButton(TPngBitBtn(FButtons[index]));
 if Assigned(FOnChangeActiveForm) then
   OnChangeActiveForm(self);
end;

procedure TFMAVertPages.AddForm(form: TForm);
var index: integer;
begin
  index := PageIndex(form);
  if index<0 then
  begin
    FForms.Add(form);
    PlaceForm(form);
    PlaceButton(form.Caption);
  end else
  begin
    PlaceForm(form);
    PushButton(FButtons[index]);
  end;
  if Assigned(FOnAddForm) then
  FOnAddForm(self);
end;

function TFMAVertPages.PageIndex(form: TForm): integer;
var i: integer;
begin
  Result := -1;
  for i := 0 to FForms.Count-1 do
  begin
    if FForms[i] = form  then
    begin
      Result := i;
      exit;
    end;
  end;
end;

procedure TFMAVertPages.PlaceForm(form: TForm);
var oldForm: TForm;
begin

  oldForm := nil;
  if FWorkPlace.ControlCount>0 then
    if FWorkPlace.Controls[0] is TForm then
    begin
      oldForm := TForm(FWorkPlace.Controls[0]);
    end;

  with form do
  begin
    Hide;
    BorderStyle := bsNone;
    Align := alClient;
    Parent := FWorkPlace;
    Show;
    SetFocus;
  end;
  FActiveForm := form;

  if Assigned(oldForm) then
  begin
      oldForm.Hide;
      oldForm.Parent := nil;
  end;

end;

procedure TFMAVertPages.PlaceButton(button_caption: string);
var button: TPngBitBtn; h: integer; rect: TRect;
begin

  rect := FVertPanel.ClientRect;
  button := TPngBitBtn.Create(self);
  FButtons.Add(button);
  PushButton(button);

  with button do
  begin
    Parent := FVertPanel;
    Align := alTop;
    TabStop := false;
    Margins.Top := 0;
    AlignWithMargins := true;
    Caption := button_caption;
    WordWrap := true;
    DrawText(Canvas.Handle, button_caption, button_caption.Length, rect, DT_CALCRECT or DT_NOPREFIX or DT_WORDBREAK);
    Height := rect.Height+20;
    OnClick := OnButtonClick;
    BevelInner := bvNone;
    BevelKind :=  bkNone;
    BevelOuter := bvNone;
    Top := 1000;
    Show;
  end;
end;

procedure TFMAVertPages.PushButton(button: TPngBitBtn);
var i: integer;
begin
  for i := 0 to FVertPanel.ControlCount-1 do
  begin
    TPngBitBtn(FVertPanel.Controls[i]).Default := false;
    TPngBitBtn(FVertPanel.Controls[i]).Repaint;
  end;
  button.Default := true;
end;

procedure TFMAVertPages.RemoveActiveForm;
begin
  RemoveForm(ActiveForm);
end;

procedure TFMAVertPages.RemoveForm(form: TForm);
var index: integer;
begin
  index := FForms.IndexOf(form);
  if index>-1 then
  begin
    form.Hide;
    form.Parent := nil;
    FForms.Delete(index);
    TPngBitBtn(FButtons[index]).Free;
    FButtons.Delete(index);
  end;
end;

procedure TFMAVertPages.RemoveForms;
var form: TForm;
begin
  for form in FForms do
  begin
    RemoveForm(form);
    form.Free;
  end;
end;

procedure TFMAVertPages.SetShowButtons(Value: boolean);
begin
  FShowButtons := Value;
  FVertPanel.Visible := FShowButtons;
end;

end.

