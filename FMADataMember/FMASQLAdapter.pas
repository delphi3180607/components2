unit FMASQLAdapter;

interface

uses System.Classes, DB, FMAClasses;

type

  IFMASQLAdapter = interface
    function GetRowSet(selectQuery: TStringList; parameters: TFMAParamList; out dataSet: TDataSet; out errorMessage: string): boolean;
    function ExecuteInsert(insertQuery: TStringList; parameters: TFMAParamList; out lastid: Variant; out errorMessage: string): boolean;
    function ExecuteUpdate(updateQuery: TStringList; parameters: TFMAParamList; out errorMessage: string): boolean;
    function ExecuteDelete(deleteQuery: TStringList; parameters: TFMAParamList; out errorMessage: string): boolean;
  end;

implementation

end.
