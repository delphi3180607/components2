unit FMADataEntity;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FMAClasses, FMADataMember;

type

  IFMADataEntity = interface
    function GetList(dest: TFDMemTable; context: TFMAContext; out errorMessage: string): boolean;
    function GetOne(dest: TFDMemTable; keyValue: Variant; Mode: TDataCopyMode; out errorMessage: string): boolean;
    function Insert(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function Delete(source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function DeleteOne(source: TFDMemTable; out errorMessage: string): boolean;
    function Update(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function QueryData(dataKind: string; dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function ExecuteAction(actionName: string; source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    procedure RegisterDataService(dataService: IFMADataMember; keyFieldName: string);
  end;

  TFMADataEntity = class(TInterfacedObject, IFMADataEntity)
    private
      FKeyFieldName: string;
      FDataService: IFMADataMember;
    public
      function GetList(dest: TFDMemTable; context: TFMAContext; out errorMessage: string): boolean;
      function GetOne(dest: TFDMemTable; keyValue: Variant; Mode: TDataCopyMode; out errorMessage: string): boolean;
      function Insert(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
      function Delete(source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
      function DeleteOne(source: TFDMemTable; out errorMessage: string): boolean;
      function Update(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
      function QueryData(dataKind: string; dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
      function ExecuteAction(actionName: string; source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
      procedure RegisterDataService(dataService: IFMADataMember; keyFieldName: string);
  end;


implementation

{ TFMADataEntity }

function TFMADataEntity.Delete(source: TFDMemTable; context: TFMAContext;
  range: TFMARangeDef; out errorMessage: string): boolean;
begin
  FDataService.Delete(source, context, range, errorMessage);
end;

function TFMADataEntity.DeleteOne(source: TFDMemTable;
  out errorMessage: string): boolean;
begin
  FDataService.DeleteOne(source, errorMessage);
end;

function TFMADataEntity.ExecuteAction(actionName: string; source: TFDMemTable;
  context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
begin

end;

function TFMADataEntity.GetList(dest: TFDMemTable; context: TFMAContext;
  out errorMessage: string): boolean;
begin
  FDataService.GetList(dest, context, errormessage);
end;

function TFMADataEntity.GetOne(dest: TFDMemTable; keyValue: Variant;
  Mode: TDataCopyMode; out errorMessage: string): boolean;
begin
  FDataService.GetOne(dest, keyValue, Mode, errorMessage);
end;

function TFMADataEntity.Insert(source, dest: TFDMemTable; context: TFMAContext;
  range: TFMARangeDef; out errorMessage: string): boolean;
begin
  FDataService.Insert(source, dest, context, range, errorMessage);
end;

function TFMADataEntity.Update(source, dest: TFDMemTable; context: TFMAContext;
  range: TFMARangeDef; out errorMessage: string): boolean;
begin
  FDataService.Update(source, dest, context, range, errorMessage);
end;

function TFMADataEntity.QueryData(dataKind: string; dest: TFDMemTable;
  context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
begin

end;

procedure TFMADataEntity.RegisterDataService(dataService: IFMADataMember;
  keyFieldName: string);
begin
  FKeyFieldName := keyFieldName;
  FDataService := dataService;
end;

end.
