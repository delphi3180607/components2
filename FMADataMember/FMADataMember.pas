unit FMADataMember;

interface

uses
  System.SysUtils, System.Classes, System.StrUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FMAClasses, FMASQLAdapter;

type

  TDataCopyMode = (cmAppend, cmUpdate);

  IFMADataMember = interface
    procedure SetKeyName(keyFieldName: string);
    function GetKeyName: string;
    function GetList(dest: TFDMemTable; context: TFMAContext; out errorMessage: string): boolean;
    function GetOne(dest: TFDMemTable; keyValue: Variant; Mode: TDataCopyMode; out errorMessage: string): boolean;
    function Insert(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function Delete(source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function DeleteOne(source: TFDMemTable; out errorMessage: string): boolean;
    function Update(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function QueryData(dataKind: string; dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function ExecuteAction(actionName: string; source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    property KeyFieldName: string read GetKeyName write SetKeyName;
  end;

  TFMASQLDataMember = class abstract(TComponent, IFMADataMember)
  private
    FKeyFieldName: string;
    FSelectSQL: TStringList;
    FInsertSQL: TStringList;
    FUpdateSQL: TStringList;
    FDeleteSQL: TStringList;
    FSQLAdapter: IFMASQLAdapter;
    procedure SetKeyName(keyFieldName: string);
    function GetKeyName: string;
  public
    function GetList(dest: TFDMemTable; context: TFMAContext; out errorMessage: string): boolean;
    function GetOne(dest: TFDMemTable; keyValue: Variant; Mode: TDataCopyMode; out errorMessage: string): boolean;
    function Insert(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function Delete(source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function DeleteOne(source: TFDMemTable; out errorMessage: string): boolean;
    function Update(source, dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function QueryData(dataKind: string; dest: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    function ExecuteAction(actionName: string; source: TFDMemTable; context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
    procedure RegisterSQLAdapter(adapter: IFMASQLAdapter);
    property KeyFieldName: string read GetKeyName write SetKeyName;
    property SelectSQL: TStringList read FSelectSQL write FSelectSQL;
    property InsertSQL: TStringList read FInsertSQL write FInsertSQL;
    property UpdateSQL: TStringList read FUpdateSQL write FUpdateSQL;
    property DeleteSQL: TStringList read FDeleteSQL write FDeleteSQL;
  end;

implementation

procedure Register;
begin
  RegisterComponents('Project Controls', [TFMASQLDataMember]);
end;

{ TFMADataMember }

{ TFMASQLDataMember }

function TFMASQLDataMember.Delete(source: TFDMemTable; context: TFMAContext;
  range: TFMARangeDef; out errorMessage: string): boolean;
var params: TFMAParamList;
begin
  params := TFMAParamList.Create;
  params.Add(FKeyFieldName, source.FieldByName(FKeyFieldName).Value);
  Result := FSQLAdapter.ExecuteDelete(FDeleteSQL, params, errormessage);
  params.Free;
end;

function TFMASQLDataMember.DeleteOne(source: TFDMemTable;
  out errorMessage: string): boolean;
var params: TFMAParamList;
begin
  params := TFMAParamList.Create;
  params.Add(FKeyFieldName, source.FieldByName(FKeyFieldName).Value);
  Result := FSQLAdapter.ExecuteDelete(FDeleteSQL, params, errormessage);
  params.Free;
end;

function TFMASQLDataMember.ExecuteAction(actionName: string;
  source: TFDMemTable; context: TFMAContext; range: TFMARangeDef;
  out errorMessage: string): boolean;
begin

end;

function TFMASQLDataMember.GetList(dest: TFDMemTable; context: TFMAContext;
  out errorMessage: string): boolean;
var params: TFMAParamList; dataset: TDataSet;
begin
  params := TFMAParamList.Create;
  Result := FSQLAdapter.GetRowSet(FSelectSQL, params, dataset, errorMessage);
  if Result then
    dest.CopyDataSet(dataset,[coStructure,coAppend,coRestart]);
  params.Free;
end;

function TFMASQLDataMember.GetOne(dest: TFDMemTable; keyValue: Variant;
  Mode: TDataCopyMode; out errorMessage: string): boolean;
var params: TFMAParamList; dataset: TDataSet;
begin
  params := TFMAParamList.Create;
  params.Add(FKeyFieldName, keyValue);
  Result := FSQLAdapter.GetRowSet(FSelectSQL, params, dataset, errorMessage);
  if Result then
  begin
    if Mode = cmAppend then
      dest.Append
    else
      dest.Edit;
    dest.CopyRecord(dataset);
    dest.Post;
  end;
  params.Free;
end;

function TFMASQLDataMember.Insert(source, dest: TFDMemTable;
  context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
var i: integer; params: TFMAParamList; lastid: Variant;
begin
  params := TFMAParamList.Create;
  for i := 0 to source.Fields.Count-1 do
  begin
    if UpperCase(source.Fields[i].Name)<>UpperCase(FKeyFieldName) then
      params.Add(source.Fields[i].Name, source.Fields[i].Value);
  end;
  Result := FSQLAdapter.ExecuteInsert(FInsertSQL, params, lastid, errormessage);
  if Result then
    GetOne(dest, lastid, cmAppend, errormessage);
end;

function TFMASQLDataMember.Update(source, dest: TFDMemTable;
  context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
var i: integer; params: TFMAParamList;
begin
  params := TFMAParamList.Create;
  for i := 0 to source.Fields.Count-1 do
  begin
    if UpperCase(source.Fields[i].Name)<>UpperCase(FKeyFieldName) then
      params.Add(source.Fields[i].Name, source.Fields[i].Value);
  end;
  Result := FSQLAdapter.ExecuteUpdate(FInsertSQL, params, errormessage);
  if Result then
    GetOne(dest, source.FieldByName(FKeyFieldName).Value, cmUpdate, errormessage);
end;

function TFMASQLDataMember.QueryData(dataKind: string; dest: TFDMemTable;
  context: TFMAContext; range: TFMARangeDef; out errorMessage: string): boolean;
begin

end;

procedure TFMASQLDataMember.RegisterSQLAdapter(adapter: IFMASQLAdapter);
begin
  FSQLAdapter := adapter;
end;

function TFMASQLDataMember.GetKeyName: string;
begin
  Result := FKeyFieldName;
end;

procedure TFMASQLDataMember.SetKeyName(keyFieldName: string);
begin
  FKeyFieldName := keyFieldName;
end;

end.
