unit FMAClasses;

interface

uses
  System.Generics.Collections;

type
  TFMAContext = class(TObject);
  TFMARangeDef = class(TObject);
  TFMAAppContext = class(TObject);

  TFMAParamList = class(TDictionary<string, Variant>);

implementation

end.
