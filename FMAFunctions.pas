unit FMAFunctions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, ComObj, Math;


const
  jsString = 'String';
  jsDate = 'Date';
  jsTime = 'Time';
  jsInteger = 'Integer';
  jsGuid = 'Guid';
  jsBoolean = 'Boolean';
  jsReal = 'Real';

function StreamToString(AData: TStream): string;
function VarToIntDef(const V: Variant; const ADefault: Integer = 0): Integer;
function VarToRealDef(const V: Variant; const ADefault: Real = 0): Real;
function VarToStrDef(const V: Variant; const ADefault: string = ''): string;
function GetOwnerForm(Component: TComponent): TForm;

implementation


function StreamToString(AData: TStream): string;
var AStr: string; nLen: Int64;
begin
    nLen := Math.Min(AData.Size, 2147483647);
    SetLength(AStr, nLen);
    AData.Seek(0, soFromBeginning);
    AData.Read(PChar(AStr)^, nLen);
    result := AStr;
end;

function VarToIntDef(const V: Variant; const ADefault: Integer = 0): Integer;
begin
  if V = NULL then
    Result := ADefault
  else
    Result := V;
end;

function VarToRealDef(const V: Variant; const ADefault: Real = 0): Real;
begin
  if V = NULL then
    Result := ADefault
  else
    Result := V;
end;


function VarToStrDef(const V: Variant; const ADefault: string = ''): string;
begin
  if V = NULL then
    Result := ADefault
  else
    Result := V;
end;


function GetOwnerForm(Component: TComponent): TForm;
var
  C: TComponent;
begin
  Result := nil;
  if Component = nil then
    exit;
  C := Component;
  repeat
    Component := C;
    try
    C := Component.Owner except C := Component;
    end;
  until (C is TForm) or (C = Component);
  if C is TForm then
    Result := C as TForm;
end;



end.