unit FMAEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.StdCtrls, DBCtrlsEh,
  System.Generics.Collections, Vcl.Mask;

type
  TFMAFormEdit = class(TForm)
    dsLocal: TDataSource;
    plBottom: TPanel;
    btOk: TButton;
    btCancel: TButton;
    procedure btOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FRequiredFields: TList<TCustomDBEditEh>;
    procedure EditChange(Sender: TObject; var Handled: Boolean);
  protected
    errormessage: string;
  public
    constructor Create(AOwner: TComponent); override;
    procedure AddRequired(control: TCustomDBEditEh);
    function CheckRequired(): boolean;
  end;

var
  FMAFormEdit: TFMAFormEdit;

implementation

{$R *.dfm}

{ TFMAFormEdit }

procedure TFMAFormEdit.AddRequired(control: TCustomDBEditEh);
begin
  FRequiredFields.Add(control);
  control.OnUpdateData := EditChange;
end;

procedure TFMAFormEdit.btOkClick(Sender: TObject);
begin
  ActiveControl := nil;
  if CheckRequired() then
    ModalResult := mrOk
  else
    ModalResult := mrNone;
end;

function TFMAFormEdit.CheckRequired: boolean;
var i: integer;
begin
  errormessage := '';
  for i:= 0 to FRequiredFields.Count-1 do
  begin
    if (FRequiredFields[i].Value = null) or (VarToStr(FRequiredFields[i].Value)='') then
    begin
      errormessage := errormessage+'�� ��������� �������� ���� :'+FRequiredFields[i].ControlLabel.Caption+#10#13;
    end;
  end;
  if errormessage <> '' then
  begin
    ShowMessage(errormessage);
    result := false;
  end else
  begin
    result := true;
  end;
end;

constructor TFMAFormEdit.Create(AOwner: TComponent);
begin
  inherited;
  FRequiredFields := TList<TCustomDBEditEh>.Create;
end;

procedure TFMAFormEdit.EditChange(Sender: TObject; var Handled: Boolean);
var i: integer;
begin
  i := FRequiredFields.IndexOf(TCustomDBEditEh(Sender));
  if i>-1 then
  begin
    if (FRequiredFields[i].Value = null) or (VarToStr(FRequiredFields[i].Value)='') then
    begin
      TDBEditEh(FRequiredFields[i]).Color := clYellow;
      TDBEditEh(FRequiredFields[i]).StyleElements := [seBorder, seFont];
      TDBEditEh(FRequiredFields[i]).Repaint;
    end else
    begin
      TDBEditEh(FRequiredFields[i]).Color := clWindow;
      TDBEditEh(FRequiredFields[i]).StyleElements := [seBorder, seClient, seFont];
      TDBEditEh(FRequiredFields[i]).Repaint;
    end;
  end;
end;

procedure TFMAFormEdit.FormShow(Sender: TObject);
var i: integer; field: TField;
begin
  if not Assigned(dsLocal.DataSet) then exit;
  FRequiredFields.Clear;
  for i := 0 to ControlCount-1 do
  begin
    if Controls[i].InheritsFrom(TCustomDBEditEh) then
    if Assigned(TDBEditEh(Controls[i]).DataSource) then
    if trim(TDBEditEh(Controls[i]).DataField)<>'' then
    begin
      field := dsLocal.DataSet.FindField(trim(TDBEditEh(Controls[i]).DataField));
      if Assigned(field) then
      if field.Required then
      begin
        AddRequired(TCustomDBEditEh(Controls[i]));
        if (TCustomDBEditEh(Controls[i]).Value = null) or (VarToStr(TCustomDBEditEh(Controls[i]).Value)='') then
        begin
          TDBEditEh(Controls[i]).Color := clYellow;
          TDBEditEh(Controls[i]).StyleElements := [seBorder, seFont];
        end else
        begin
          TDBEditEh(Controls[i]).Color := clWindow;
          TDBEditEh(Controls[i]).StyleElements := [seBorder, seClient, seFont];
        end;
      end;
    end;
  end;
end;

end.
