unit FMACrudDisp;

interface

uses
  System.SysUtils, System.Classes, DB, JSON, Vcl.forms, Vcl.controls, Vcl.dialogs,
  fmaedit, FMACrudAgent, FMACrudAgentInterface, MemTableDataEh, MemTableEh,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, DBCtrlsEh, DBVertGridsEh, Vcl.StdCtrls,
  System.UITypes, FMAParamTransform, WinAPI.Windows, WinApi.Messages;

type

  TFMAField = class(TNamedItem)
  private
    FPrecision: Integer;
    FFieldNo: Integer;
    FSize: Integer;
    FDataType: TFieldType;
    FAttributes: TFieldAttributes;
    FLongCaption: string;
    FShortCaption: string;
    FKeyFieldName: string;
  published
    property Precision: Integer read FPrecision write FPrecision;
    property FieldNo: Integer read FFieldNo write FFieldNo;
    property Size: Integer read FSize write FSize;
    property DataType: TFieldType read FDataType write FDataType;
    property Attributes: TFieldAttributes read FAttributes write FAttributes;
    property LongCaption: string read FLongCaption write FLongCaption;
    property ShortCaption: string read FShortCaption write FShortCaption;
    property KeyFieldName: string read FKeyFieldName write FKeyFieldName;
  end;

  TFMAFields = class(TOwnedCollection)
  private
    function GetItems(Index: Integer): TFMAField;
    procedure SetItems(Index: Integer; const Value: TFMAField);
  public
    constructor Create(AOwner: TPersistent);
    property Items[Index: Integer]: TFMAField read GetItems write SetItems; default;
  end;

  TFMACRUDDisp = class(TComponent)
  private
    FFields: TFMAFields;
    FGrid: TDBGridEh;
    FMemData: TMemTableEh;
    FFormEdit: TForm;
    FCrudAgent: IFMACrudAgent;
    FEditFormDataSource: TDataSource;
    FOnBeforeGetList: TNotifyEvent;
    FOnBeforeInsert: TNotifyEvent;
    FOnBeforeEdit: TNotifyEvent;
    FOnBeforeDelete: TNotifyEvent;
    FOnAfterInsert: TNotifyEvent;
    FOnAfterEdit: TNotifyEvent;
    FOnAfterDelete: TNotifyEvent;
    FOnAfterGetList: TNotifyEvent;
    FOnBeforeMultipleInsert: TNotifyEvent;
    FOnMultipleInsert: TNotifyEvent;
    FOldAfterScroll: TDataSetNotifyEvent;
    FReadOnly: boolean;
    FOnCheckState: TNotifyEvent;
  protected
    FEmptyStructure: boolean;
    procedure SimulateKeystroke(Key: byte; extra: DWORD);
    procedure RegisterEditForm(form: TForm);
    function MakeControl(field: TFMAField; form: TForm): TCustomDBEditEh;
    procedure SetEditFormDataSource(value: TDataSource);
    procedure SetReadOnly(value: boolean);
    procedure ApplyAttributes(var ds: TMemTableEh);
    procedure DisableMultipleField(FormEdit: TForm; multiple_field_name: string);
    procedure EnableMultipleField(FormEdit: TForm; multiple_field_name: string);
    procedure Loaded; override;
  public
    Terminate: boolean;
    constructor Create(AOwner: TComponent); override;
    function GetList(dest: TMemTableEh; out errormessage: string): boolean;
    procedure Insert;
    procedure InsertFromSource(source: TDataSet);
    procedure MultipleInsert(multiple_field_name, source_field_name: string; multiple_dataset: TDataSet);
    procedure Edit;
    procedure Delete;
    procedure InnerSave;
    procedure InnerInsert;
    procedure CreateAllColumns;
    procedure CreateEditFormFields;
    procedure CreateAllColumnsAndEditFields;
    procedure ApplyMasterDetailParams(MasterCrudDisp: TFMACRUDDisp; DetailFieldName, MasterFieldName: string);
    property FormEdit: TForm read FFormEdit;
  published
    property Fields: TFMAFields read FFields write FFields;
    property Grid: TDBGridEh read FGrid write FGrid;
    property MemData: TMemTableEh read FMemData write FMemData;
    property CrudAgent: IFMACrudAgent read FCrudAgent write FCrudAgent;
    property ReadOnly: boolean read FReadOnly write SetReadOnly;
    property OnCheckState: TNotifyEvent read FOnCheckState write FOnCheckState;
    property EditFormDataSource: TDataSource read FEditFormDataSource write SetEditFormDataSource;
    property OnBeforeGetList: TNotifyEvent read FOnBeforeGetList write FOnBeforeGetList;
    property OnBeforeInsert: TNotifyEvent read FOnBeforeInsert write FOnBeforeInsert;
    property OnBeforeEdit: TNotifyEvent read FOnBeforeEdit write FOnBeforeEdit;
    property OnBeforeDelete: TNotifyEvent read FOnBeforeDelete write FOnBeforeDelete;
    property OnAfterInsert: TNotifyEvent read FOnAfterInsert write FOnAfterInsert;
    property OnAfterEdit: TNotifyEvent read FOnAfterEdit write FOnAfterEdit;
    property OnAfterDelete: TNotifyEvent read FOnAfterDelete write FOnAfterDelete;
    property OnAfterGetList: TNotifyEvent read FOnAfterGetList write FOnAfterGetList;
    property OnBeforeMultipleInsert: TNotifyEvent read FOnBeforeMultipleInsert write FOnBeforeMultipleInsert;
    property OnMultipleInsert: TNotifyEvent read FOnMultipleInsert write FOnMultipleInsert;
  end;

procedure Register;

implementation

uses DBSQLLookUp;

procedure Register;
begin
  RegisterComponents('Project Controls', [TFMACRUDDisp]);
end;


{ TFMAFields }

constructor TFMAFields.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner, TFMAField);
end;

function TFMAFields.GetItems(Index: Integer): TFMAField;
begin
  Result := TFMAField(inherited Items[Index]);
end;

procedure TFMAFields.SetItems(Index: Integer; const Value: TFMAField);
begin
  Items[Index] := Value;
end;

{ TFMACRUDDisp }


procedure TFMACRUDDisp.ApplyAttributes(var ds: TMemTableEh);
var i: integer; f: TField;
begin
  for i := 0 to FFields.Count-1 do
  begin
    f := ds.FindField(FFields[i].Name);
    if Assigned(f) then
    begin
      if faRequired in FFields[i].Attributes then
      f.Required := true;
    end;
  end;
end;

procedure TFMACRUDDisp.ApplyMasterDetailParams(MasterCrudDisp: TFMACRUDDisp; DetailFieldName, MasterFieldName: string);
begin
  if not Assigned(FCrudAgent) then exit;
  FCrudAgent.ParamTransform.ResetContext;
  FCrudAgent.ParamTransform.AddPatternContext('/*wherefilter*/', DetailFieldName+'= :'+MasterFieldName);
  FCrudAgent.ParamTransform.AddParameterContext(MasterFieldName, MasterCrudDisp.MemData.FieldByName(MasterFieldName).DataType, MasterCrudDisp.MemData.FieldByName(MasterFieldName).Value);
end;

constructor TFMACRUDDisp.Create(AOwner: TComponent);
begin
  inherited;
  FFields := TFMAFields.Create(self);
  FEmptyStructure := true;
  Terminate := false;
  ReadOnly := false;
end;

procedure TFMACRUDDisp.CreateAllColumns;
var field: TFMAField; c: TColumnEh; i: integer;
begin
  if Assigned(FGrid) then
  begin
    TDBGridEh(FGrid).Columns.Clear;
    for i := 0 to FFields.Count-1 do
    begin
      field := FFields.Items[i];
      if not (faHiddenCol in field.Attributes) then
      begin
        c := TDBGridEh(FGrid).Columns.Add;
        c.AutoFitColWidth := false;
        c.Width := 150;
        c.FieldName := field.Name;
        c.Title.Caption := field.ShortCaption;
        c.Visible := true;
      end;
    end;
  end;
end;

procedure TFMACRUDDisp.CreateAllColumnsAndEditFields;
begin
  CreateAllColumns;
  CreateEditFormFields;
end;

procedure TFMACRUDDisp.Insert;
var errormessage: string; result_state: boolean;
begin
  if Assigned(FOnCheckState) then
  FOnCheckState(self);
  if FReadOnly then exit;
  FMemData.Append;
  if Assigned(FOnBeforeInsert) then
  FOnBeforeInsert(self);
  if Assigned(FFormEdit) then
  begin
    if Assigned(FEditFormDataSource) then
    FEditFormDataSource.DataSet := FMemData;
    FFormEdit.ShowModal;
    if FFormEdit.ModalResult = mrOk then
    begin
      if Assigned(FCrudAgent) then
      if FCrudAgent.Insert(FMemData, errormessage) then
      begin
        FMemData.Post;
        if Assigned(FOnAfterInsert) then
        begin
          FOnAfterInsert(self);
          if Assigned(MemData.AfterScroll) then
          MemData.AfterScroll(MemData);
        end;
      end else
      begin
        FMemData.Cancel;
      end;
    end else
    begin
        FMemData.Cancel;
    end;

    if Assigned(FEditFormDataSource) then
    FEditFormDataSource.DataSet := nil;

    if Assigned(Grid) then
    FGrid.SetFocus;

    SimulateKeystroke(VK_ESCAPE, 0);
  end;
end;

procedure TFMACRUDDisp.Loaded;
begin
  inherited;
  if Assigned(FGrid) then FGrid.ReadOnly := FReadOnly;
end;

procedure TFMACRUDDisp.InsertFromSource(source: TDataSet);
var errormessage: string; result_state: boolean;
begin
  if Assigned(FOnCheckState) then
  FOnCheckState(self);
  if FReadOnly then exit;
  FMemData.EmptyTable;
  FMemData.LoadFromDataSet(source,1,lmAppend,false);
  FMemData.Edit;
  if Assigned(FOnBeforeInsert) then
  FOnBeforeInsert(self);
  FCrudAgent.Insert(FMemData, errormessage);
  FMemData.Cancel;
end;

procedure TFMACRUDDisp.MultipleInsert(multiple_field_name, source_field_name: string; multiple_dataset: TDataSet);
var errormessage: string; result_state: boolean;
begin
  if not Assigned(FMemData) then exit;
  if not Assigned(FCrudAgent) then exit;
  if Assigned(FOnCheckState) then
  FOnCheckState(self);
  if FReadOnly then exit;
  Terminate := false;
  FMemData.Append;
  if Assigned(FOnBeforeInsert) then
  FOnBeforeInsert(self);
  if Assigned(FFormEdit) then
  begin
    if Assigned(FEditFormDataSource) then
    FEditFormDataSource.DataSet := FMemData;
    FMemData.FieldByName(multiple_field_name).Value := multiple_dataset.FieldByName(source_field_name).Value;
    DisableMultipleField(FFormEdit, multiple_field_name);
    FFormEdit.ShowModal;
    EnableMultipleField(FFormEdit, multiple_field_name);
    if FFormEdit.ModalResult = mrOk then
    begin
      if Assigned(FOnBeforeMultipleInsert) then
      FOnBeforeMultipleInsert(self);
      if Terminate then
      begin
        FMemData.Cancel;
        if Assigned(FEditFormDataSource) then
        FEditFormDataSource.DataSet := nil;
        GetList(nil, errormessage);
        exit;
      end;
      multiple_dataset.First;
      while not multiple_dataset.Eof do
      begin
        FMemData.LoadFromDataSet(FMemData,1,lmCopy,false);
        FMemData.FieldByName(multiple_field_name).Value := multiple_dataset.FieldByName(source_field_name).Value;
        if FCrudAgent.Insert(FMemData, errormessage) then
        begin
          if Assigned(FOnMultipleInsert) then
          FOnMultipleInsert(self);
        end;
        multiple_dataset.Next;
      end;
    end;
    if Assigned(FEditFormDataSource) then
    FEditFormDataSource.DataSet := nil;
    GetList(nil, errormessage);

    if Assigned(Grid) then
    FGrid.SetFocus;

  end;
end;

procedure TFMACRUDDisp.Delete;
var errormessage: string; result_state: boolean;
begin
   if not Assigned(FMemData) then exit;
   if not Assigned(FCrudAgent) then exit;
   if Assigned(FOnCheckState) then
   FOnCheckState(self);
   if FReadOnly then exit;
   if MessageDlg('������� ������?',mtCustom, mbYesNo, 0)=mrYes then
   begin
    if Assigned(FOnBeforeDelete) then
    FOnBeforeDelete(self);
    if Assigned(FCrudAgent) then
    begin
      if FCrudAgent.Delete(FMemData, errormessage) then
        FMemData.Delete
    end;
    if Assigned(FOnAfterDelete) then
    FOnAfterDelete(self);
   end;
end;

procedure TFMACRUDDisp.Edit;
var errormessage: string; result_state: boolean;
begin
  if not Assigned(FMemData) then exit;
  if not Assigned(FCrudAgent) then exit;
  if Assigned(FOnCheckState) then
  FOnCheckState(self);
  if FReadOnly then exit;
  if Assigned(FOnBeforeEdit) then
  FOnBeforeEdit(self);
  FMemData.Edit;
  if Assigned(FFormEdit) then
  begin
    if Assigned(FEditFormDataSource) then
    FEditFormDataSource.DataSet := FMemData;
    FFormEdit.ShowModal;
    if FFormEdit.ModalResult = mrOk then
    begin
      if Assigned(FCrudAgent) then
      if FCrudAgent.Update(FMemData, errormessage) then
      begin
        FMemData.Post
      end else
      begin
        FMemData.Cancel;
      end;
      if Assigned(FOnAfterEdit) then
      FOnAfterEdit(self);
    end else
    begin
      FMemData.Cancel;
    end;

    if Assigned(FEditFormDataSource) then
    FEditFormDataSource.DataSet := nil;

    if Assigned(Grid) then
    FGrid.SetFocus;

    SimulateKeystroke(VK_ESCAPE, 0);
  end;
end;

procedure TFMACRUDDisp.DisableMultipleField(FormEdit: TForm; multiple_field_name: string);
var i: integer;
begin
  for i := 0 to FormEdit.ControlCount-1 do
  begin
    if FormEdit.Controls[i].InheritsFrom(TCustomDBEditEh) then
    if TCustomDBEditEh(FormEdit.Controls[i]).DataField=multiple_field_name then
    FormEdit.Controls[i].Enabled := false;
  end;
end;

procedure TFMACRUDDisp.EnableMultipleField(FormEdit: TForm; multiple_field_name: string);
var i: integer;
begin
  for i := 0 to FormEdit.ControlCount-1 do
  begin
    if FormEdit.Controls[i].InheritsFrom(TCustomDBEditEh) then
    if TCustomDBEditEh(FormEdit.Controls[i]).DataField=multiple_field_name then
    FormEdit.Controls[i].Enabled := true;
  end;
end;

function TFMACRUDDisp.GetList(dest: TMemTableEh; out errormessage: string): boolean;
var ds: TDataSet; current_dest: TMemTableEh;
begin

  result := true;
  if not Assigned(FCrudAgent) then exit;

  if Assigned(FOnCheckState) then
  FOnCheckState(self);

  try
    Screen.Cursor := crHourGlass;

    if Assigned(FOnBeforeGetList) then
    FOnBeforeGetList(self);

    if dest=nil then
      current_dest := FMemData
    else
    begin
      current_dest := dest;
      FEmptyStructure := true;
    end;

    if not Assigned(current_dest) then exit;

    if Assigned(current_dest.AfterScroll) then
    begin
      FOldAfterScroll := current_dest.AfterScroll;
      current_dest.AfterScroll := nil;
    end;

    if Assigned(FCrudAgent) then
    begin
      ds := FCrudAgent.GetList(result, errormessage);
      if not Assigned(ds) then
      begin
        Screen.Cursor := crDefault;
        exit;
      end;
      if FEmptyStructure then
      begin
        FEmptyStructure := false;
      end;
      current_dest.EmptyTable;
      current_dest.Close;
      current_dest.LoadFromDataSet(ds,0,lmCopy, false);
      current_dest.Open;
      ApplyAttributes(current_dest);

    end else
      current_dest.Open;

    if Assigned(FOnAfterGetList) then
    FOnAfterGetList(self);

    if Assigned(FOldAfterScroll) then
    current_dest.AfterScroll := FOldAfterScroll;

    if Assigned(current_dest.AfterScroll) then
    current_dest.AfterScroll(current_dest);

    if dest<>nil then
      FEmptyStructure := true;

  except
    on E: Exception do
    begin
      Screen.Cursor := crDefault;
      ShowMessage(E.Message);
      Application.Terminate;
    end;
  end;

  Screen.Cursor := crDefault;
end;

procedure TFMACRUDDisp.RegisterEditForm(form: TForm);
begin
  FFormEdit := form;
end;

procedure TFMACRUDDisp.InnerInsert;
var errormessage: string;
begin
  if not Assigned(FMemData) then exit;
  if not Assigned(FCrudAgent) then exit;
  if Assigned(FOnCheckState) then
  FOnCheckState(self);
  if FReadOnly then exit;
  if Assigned(FOnBeforeInsert) then
  FOnBeforeInsert(self);
  if Assigned(FCrudAgent) then
  if FCrudAgent.Insert(FMemData, errormessage) then
  begin
    FMemData.Post
  end else
  begin
    FMemData.Cancel;
  end;
  if Assigned(FOnAfterInsert) then
  FOnAfterInsert(self);
end;

procedure TFMACRUDDisp.InnerSave;
var errormessage: string;
begin
  if not Assigned(FMemData) then exit;
  if not Assigned(FCrudAgent) then exit;
  if Assigned(FOnCheckState) then
  FOnCheckState(self);
  if FReadOnly then exit;
  if Assigned(FOnBeforeEdit) then
  FOnBeforeEdit(self);
  FMemData.Edit;
  if Assigned(FCrudAgent) then
  if FCrudAgent.Update(FMemData, errormessage) then
  begin
    FMemData.Post
  end else
  begin
    FMemData.Cancel;
  end;
  if Assigned(FOnAfterEdit) then
  FOnAfterEdit(self);
end;

procedure TFMACRUDDisp.SetEditFormDataSource(value: TDataSource);
begin
  FEditFormDataSource := value;
  RegisterEditForm(TForm(FEditFormDataSource.Owner));
end;

procedure TFMACRUDDisp.SetReadOnly(value: boolean);
begin
  FReadOnly := value;
  if Assigned(FGrid) then FGrid.ReadOnly := FReadOnly;
end;

procedure TFMACRUDDisp.SimulateKeystroke(Key: byte; extra: DWORD);
begin
  keybd_event(Key, extra, 0, 0);
  keybd_event(Key, extra, KEYEVENTF_KEYUP, 0);
end;

procedure TFMACRUDDisp.CreateEditFormFields;
var i: integer; f: TForm; c: TCustomDBEditEh; x,y: integer;
begin
  if not Assigned(self.FEditFormDataSource) then exit;
  if not Assigned(FEditFormDataSource) then exit;

  f := TForm(FEditFormDataSource.Owner);
  x := 10;
  y := 25;

  for i := 0 to Fields.Count-1 do
  begin
    if faHiddenCol in Fields[i].Attributes then
    continue;
    c := MakeControl(Fields[i], f);
    if Assigned(c) then
    begin
      c.Parent := f;
      c.Top := y;
      c.Left := x;
      c.Show;
      y := y+50;
    end;
  end;

end;

function TFMACRUDDisp.MakeControl(field: TFMAField; form: TForm): TCustomDBEditEh;
var c: TCustomDBEditEh;
begin
  Result := nil;

  if (faHiddenCol in field.Attributes) then exit;

  if Assigned(form.FindChildControl('ed'+uppercase(field.Name))) then
  exit;

  if field.DataType in [TFieldType.ftString, TFieldType.ftWideString] then
  begin
    if trim(field.KeyFieldName)='' then
      c := TDbEditEh.Create(form)
    else
    begin
      c := TDBSQLLookUp.Create(form);
      TDBSQLLookUp(c).KeyFieldName := 'id';
      TDBSQLLookUp(c).DisplayFieldName := 'name';
    end;
  end
  else if field.DataType in [TFieldType.ftInteger, TFieldType.ftSmallint] then
    c := TDbNumberEditEh.Create(form)
  else if field.DataType in [TFieldType.ftDate, TFieldType.ftDateTime] then
    c := TDbDateTimeEditEh.Create(form)
  else
    c := TDbEditEh.Create(form);

  c.Parent := form;

  if trim(field.KeyFieldName)='' then
    c.DataField := field.Name
  else
    TDBSQLLookUp(c).DataField := trim(field.KeyFieldName);

  c.Name := 'ed'+uppercase(field.Name);
  c.ControlLabel.Caption := field.LongCaption;
  c.ControlLabel.Layout := tlTop;
  c.ControlLabel.Visible := true;
  c.DataSource := self.FEditFormDataSource;

  Result := c;
end;

end.
