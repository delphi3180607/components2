unit FMACrudDispEditor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.ToolWin,
  Vcl.StdCtrls, System.ImageList, Vcl.ImgList,
  DesignIntf, DesignEditors,
  FMACRUDDisp, DB;

type


  TFMACrudDispEditor = class(TComponentEditor)
  public
    procedure CreateAllFields;
    procedure Edit; override;
    function GetVerb(Index: Integer): string; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerbCount: Integer; override;
  end;

procedure Register;

implementation

procedure TFMACrudDispEditor.CreateAllFields;
var i: integer; ds: TDataSet; field: TFMAField; errormessage: string;
begin

  if TFMACRUDDisp(Component).Fields.Count>0 then
  exit;

  if not TFMACRUDDisp(Component).GetList(nil, errormessage) then
  begin
    ShowMessage(errormessage);
    exit;
  end;

  ds := TFMACRUDDisp(Component).MemData;

  for i := 0 to ds.FieldCount-1 do
  begin
    field := TFMAField(TFMACRUDDisp(Component).Fields.Add);
    field.Precision := ds.FieldDefs[i].Precision;
    field.FieldNo := ds.FieldDefs[i].FieldNo;
    field.Size := ds.FieldDefs[i].Size;
    field.DataType := ds.FieldDefs[i].DataType;
    field.Attributes := ds.FieldDefs[i].Attributes;
    field.Name := ds.FieldDefs[i].Name;
  end;
end;

procedure TFMACrudDispEditor.Edit;
begin
  //
end;

function TFMACrudDispEditor.GetVerb(Index: Integer): string;
begin
 case Index of
    0: Result := 'Create all fields';
    1: Result := 'Create all columns and edit fields';
 end;
end;

procedure TFMACrudDispEditor.ExecuteVerb(Index: Integer);
begin
 inherited;
 case Index of
    0: CreateAllFields;
    1: TFMACRUDDisp(Component).CreateAllColumnsAndEditFields;
 end;
 Designer.Modified;
end;

function TFMACrudDispEditor.GetVerbCount: Integer;
begin
  Result := 2;
end;

procedure Register;
begin
  RegisterComponentEditor(TFMACRUDDisp, TFMACrudDispEditor);
end;


end.
