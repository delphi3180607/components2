object FMAFormEdit: TFMAFormEdit
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'FMAFormEdit'
  ClientHeight = 434
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poMainFormCenter
  StyleElements = [seFont, seClient]
  OnShow = FormShow
  TextHeight = 17
  object plBottom: TPanel
    Left = 0
    Top = 391
    Width = 602
    Height = 43
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btOk: TButton
      AlignWithMargins = True
      Left = 399
      Top = 8
      Width = 97
      Height = 32
      Margins.Top = 8
      Align = alRight
      Caption = #1047#1072#1087#1080#1089#1072#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = btOkClick
    end
    object btCancel: TButton
      AlignWithMargins = True
      Left = 502
      Top = 8
      Width = 97
      Height = 32
      Margins.Top = 8
      Align = alRight
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
  end
  object dsLocal: TDataSource
    Left = 184
    Top = 88
  end
end
