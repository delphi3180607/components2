unit FMAParamTransform;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  System.DateUtils, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, ComObj, Math,
  JSON, DB, FMAFunctions;

type
  TFMAParamTransform = class(TComponent)
  private
    FProcessed: boolean;
    procedure AddContext(key: string; value: TJSONObject); overload;
    procedure AddContext(key: string; pair_key: string; pair_value: string); overload;
  public
    Context: TJSONObject;

    constructor Create(AOwner: TComponent); override;

    function MakeJSONValue(field_type: TFieldType; field_value: Variant): TJSONObject;
    function MakeJSONParam(field_name: string; field_type: TFieldType; field_value: Variant): TJSONObject;
    function DataSetToJSONParams(source: TDataSet): TJSONObject;
    procedure JSONToDataSet(source: TJSONObject; var dest: TDataSet);

    function FieldToJSON(field_type: TFieldType; field_value: Variant): TJSONObject;
    function JSONtoVariant(source: TJSONObject): Variant;
    function JSONToDataType(source: TJSONObject): TFieldType;

    procedure ReplacePattern(var sqltext: string; pattern_pair: TJSONObject);
    procedure SetText(var text: string; text_pair: TJSONObject);
    procedure DeleteUnusedPatterns(var sql: string);

    procedure ResetContext();
    procedure AddPatternContext(pattern: string; expression: string);
    procedure AddSQLTextContext(expression: string);
    procedure AddParameterContext(param_name: string; param_type: TFieldType; param_value: Variant);
    procedure AddParametersFromDataSet(source: TDataSet);

    property Processed: boolean read FProcessed write FProcessed;
  end;

procedure Register;

implementation

{ TFMAParamTransform }

procedure TFMAParamTransform.AddContext(key, pair_key, pair_value: string);
var pair: TJSONObject;
begin
  pair := TJSONObject.Create;
  pair.AddPair(pair_key, pair_value);
  Context.AddPair(key, pair);
end;

procedure TFMAParamTransform.AddContext(key: string; value: TJSONObject);
begin
  Context.AddPair(key, value);
end;

procedure TFMAParamTransform.AddParameterContext(param_name: string; param_type: TFieldType; param_value: Variant);
var param_object: TJSONObject;
begin
  param_object := MakeJSONParam(param_name, param_type, param_value);
  AddContext('parameter', param_object);
end;

procedure TFMAParamTransform.AddParametersFromDataSet(source: TDataSet);
var i: integer;
begin
  source.First;
  for i := 0 to source.Fields.Count-1 do
  begin
    AddParameterContext(source.Fields[i].FieldName, source.Fields[i].DataType, source.Fields[i].Value);
    source.Next;
  end;
end;

procedure TFMAParamTransform.AddPatternContext(pattern, expression: string);
begin
  AddContext('pattern', pattern, expression);
end;

procedure TFMAParamTransform.AddSQLTextContext(expression: string);
begin
  AddContext('sqltext', 'sqltext', expression);
end;

constructor TFMAParamTransform.Create(AOwner: TComponent);
begin
  inherited;
  ResetContext;
end;

function TFMAParamTransform.DataSetToJSONParams(source: TDataSet): TJSONObject;
var i: integer;
begin
  result := TJSONObject.Create;
  for i := 0 to source.FieldCount-1 do
  begin
    result.AddPair(source.Fields[i].FieldName, MakeJSONValue(source.Fields[i].DataType, source.Fields[i].Value));
  end;
end;

procedure TFMAParamTransform.DeleteUnusedPatterns(var sql: string);
var p1, p2: integer;
begin
  p1 := pos('/*',sql);
  while p1>0 do
  begin
    p2 := pos('*/', sql, p1);
    if p2=0 then p2 := Length(sql);
    System.Delete(sql, p1, p2-p1+2);
    p1 := pos('/*',sql);
  end;
end;

function TFMAParamTransform.FieldToJSON(field_type: TFieldType; field_value: Variant): TJSONObject;
var valstr: string; valint: integer; valdbl: double; valbool: boolean;
begin
    result := nil;

    if field_type in [ftString, ftWideString, ftWideMemo] then
    begin
      valstr := VarToStrDef(field_value);
      result := TJSONObject.Create;
      result.AddPair(jsString, valstr);
    end else
    if field_type in [ftDate, ftDateTime] then
    begin
      if field_value = null then
      valint := 0
      else
      valint := DateTimeToTimeStamp(TDateTime(field_value)).Date;
      result := TJSONObject.Create;
      result.AddPair(jsDate, valint);
    end else
    if field_type in [ftSmallInt, ftInteger, ftLargeint, ftAutoInc] then
    begin
      valint := VarToIntDef(field_value);
      result := TJSONObject.Create;
      result.AddPair(jsInteger, valint);
    end else
    if field_type in [ftGuid] then
    begin
      valstr := field_value;
      result := TJSONObject.Create;
      result.AddPair(jsGuid, valstr);
    end;
    if field_type in [ftBoolean] then
    begin
      valbool := field_value;
      result := TJSONObject.Create;
      result.AddPair(jsBoolean, valbool);
    end else
    if field_type in [ftBCD] then
    begin
      valdbl := VarToRealDef(field_value);
      result := TJSONObject.Create;
      result.AddPair(jsReal, valdbl);
    end;
end;

procedure TFMAParamTransform.JSONToDataSet(source: TJSONObject; var dest: TDataSet);
var i: integer; p: TJSONPair; field_name: string; field: TField;
begin
  for i := 0 to source.Count-1 do
  begin
    p := source.Pairs[i];
    field_name := p.JsonString.AsType<string>;
    field := dest.FindField(field_name);
    if Assigned(field) then
    begin
      field.Value := JSONToVariant(TJSONObject(p.JsonValue));
    end;
  end;
end;

function TFMAParamTransform.JSONToDataType(source: TJSONObject): TFieldType;
var p: TJSONPair; field_type: string;
begin
  p := source.Pairs[0];
  field_type := p.JsonString.AsType<string>;

  if field_type = jsString then
  begin
    result := ftString;
  end else
  if field_type = jsDate then
  begin
    result := ftDate;
  end else
  if field_type = jsTime then
  begin
    result := ftTime;
  end else
  if field_type = jsInteger then
  begin
    result := ftInteger;
  end else
  if field_type = jsGuid then
  begin
    result := ftGUID;
  end else
  if field_type = jsBoolean then
  begin
    result := ftBoolean;
  end else
  if field_type = jsReal then
  begin
    result := ftBCD;
  end;
end;

function TFMAParamTransform.JSONtoVariant(source: TJSONObject): Variant;
var p: TJSONPair; field_type: string;
intvalue: integer; value: TJSONValue;
t: TTimeStamp;
begin
  p := source.Pairs[0];
  field_type := p.JsonString.AsType<string>;
  result := NULL;

  if field_type = jsString then
  begin
    value := p.JsonValue;
    if value.AsType<string> = '' then
      result := null
    else
      result := value.AsType<string>;
  end else
  if field_type = jsTime then
  begin
    value := p.JsonValue;
    intvalue := value.AsType<integer>;
    if intvalue = 0 then
      result := null
    else begin
      //t.Date := 1;
      //t.Time := intvalue;
      //result := TimeStampToDateTime(t);
      result := UnixToDateTime(intvalue div 1000);
      //result := intvalue;
    end;
  end else
  if field_type = jsDate then
  begin
    value := p.JsonValue;
    intvalue := value.AsType<integer>;
    if intvalue = 0 then
      result := null
    else begin
      t.Time := 0;
      t.Date := intvalue;
      result := TimeStampToDateTime(t);
    end;
  end else
  if field_type = jsInteger then
  begin
    value := p.JsonValue;
    if value.AsType<integer> = 0 then
      result := null
    else
      result := value.AsType<integer>;
  end else
  if field_type = jsGuid then
  begin
    value := p.JsonValue;
    if value.AsType<string> = '' then
      result := null
    else
      result := value.AsType<string>;
  end else
  if field_type = jsBoolean then
  begin
    value := p.JsonValue;
    result := value.AsType<boolean>;
  end else
  if field_type = jsReal then
  begin
    value := p.JsonValue;
    result := value.AsType<Real>;
  end;
end;

function TFMAParamTransform.MakeJSONParam(field_name: string; field_type: TFieldType; field_value: Variant): TJSONObject;
begin
  result := TJSONObject.Create;
  result.AddPair(field_name, MakeJSONValue(field_type, field_value));
end;

function TFMAParamTransform.MakeJSONValue(field_type: TFieldType; field_value: Variant): TJSONObject;
var valstr: string; valint: integer; valdbl: double; valbool: boolean;
begin
    result := nil;

    if field_type in [ftString, ftWideString, ftWideMemo] then
    begin
      valstr := VarToStrDef(field_value);
      result := TJSONObject.Create;
      result.AddPair(jsString, valstr);
    end else
    if field_type in [ftTime] then
    begin
      if field_value = null then
      valint := 0
      else
      valint := DateTimeToTimeStamp(TDateTime(field_value)).Time;
      result := TJSONObject.Create;
      result.AddPair(jsTime, valint);
    end else
    if field_type in [ftTimeStamp] then
    begin
      if field_value = null then
      valint := 0
      else
      valint := DateTimeToTimeStamp(TDateTime(field_value)).Date;
      result := TJSONObject.Create;
      result.AddPair(jsDate, valint);
    end else
    if field_type in [ftDate, ftDateTime] then
    begin
      if field_value = null then
      valint := 0
      else
      valint := DateTimeToTimeStamp(TDateTime(field_value)).Date;
      result := TJSONObject.Create;
      result.AddPair(jsDate, valint);
    end else
    if field_type in [ftSmallInt, ftInteger, ftLargeint, ftAutoInc] then
    begin
      valint := VarToIntDef(field_value);
      result := TJSONObject.Create;
      result.AddPair(jsInteger, valint);
    end else
    if field_type in [ftGuid] then
    begin
      valstr := VarToStr(field_value);
      result := TJSONObject.Create;
      result.AddPair(jsGuid, valstr);
    end;
    if field_type in [ftBoolean] then
    begin
      valbool := field_value;
      result := TJSONObject.Create;
      result.AddPair(jsBoolean, valbool);
    end else
    if field_type in [ftBCD] then
    begin
      valdbl := VarToRealDef(field_value);
      result := TJSONObject.Create;
      result.AddPair(jsReal, valdbl);
    end;
end;

procedure TFMAParamTransform.ReplacePattern(var sqltext: string; pattern_pair: TJSONObject);
var s: integer; pair: TJSONPair; expression_type: string; expression: string;
begin
  pair := pattern_pair.Pairs[0];
  expression_type := pair.JsonString.AsType<string>;
  expression := pair.JsonValue.AsType<string>;
  s := pos(expression_type, sqltext);
  if expression_type='/*wherefilter*/' then
  begin
    if FProcessed then
      sqltext := copy(sqltext,0,s-1)+' and ( '+expression+') '+copy(sqltext,s, length(sqltext))
    else
      sqltext := copy(sqltext,0,s-1)+' where ('+expression+') '+copy(sqltext,s, length(sqltext));
    FProcessed := true;
  end else
  if expression_type='/*orderby*/' then
  begin
    sqltext := StringReplace(sqltext, expression_type, ' order by '+expression, []);
  end else
  if expression_type='/*limit*/' then
  begin
    sqltext := StringReplace(sqltext, expression_type, ' limit '+expression, []);
  end;

end;

procedure TFMAParamTransform.ResetContext;
begin
  if Assigned(Context) then Context.Free;
  Context := TJSONObject.Create;
end;

procedure TFMAParamTransform.SetText(var text: string; text_pair: TJSONObject);
var s: integer; pair: TJSONPair; expression_type: string; expression: string;
begin
  pair := text_pair.Pairs[0];
  text := pair.JsonValue.AsType<string>;
end;

procedure Register;
begin
  RegisterComponents('Project Controls', [TFMAParamTransform]);
end;



end.
