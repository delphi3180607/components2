unit FMACrudAgent;

interface

uses
  System.SysUtils, System.Classes, DB, JSON, FMACrudAgentInterface, VCL.Dialogs, Generics.Collections, Data.SqlExpr, Variants,
  FMAFunctions, FMAParamTransform;

type

  TFMADataSetDelegate = procedure(var DataSet: TDataSet; out result_state: boolean; out ErrorMessage: string) of object;

  TFMACrudAgent = class(TComponent, IFMACrudAgent)
  private
    FBuffer: TStringStream;
    FActive: boolean;
    FAvoidEvents: boolean;
    FServerMethodGetList: TSqlServerMethod;
    FOnBeforeGetList: TNotifyEvent;
    FOnBeforeInsert: TFMADataSetDelegate;
    FOnBeforeEdit: TFMADataSetDelegate;
    FOnBeforeDelete: TFMADataSetDelegate;
    FInsertExecutor: TFMADataSetDelegate;
    FEditExecutor: TFMADataSetDelegate;
    FDeleteExecutor: TFMADataSetDelegate;
    FOnAfterInsert: TFMADataSetDelegate;
    FOnAfterEdit: TFMADataSetDelegate;
    FOnAfterDelete: TFMADataSetDelegate;
    FParamTransform: TFMAParamTransform;
    function GetParamTransform: TFMAParamTransform;
    function GetAvoidEvents: boolean;
    procedure SetAvoidEvents(value: boolean);
  protected
    procedure SetActive(value: boolean);
  public
    constructor Create(AOwner: TComponent); override;
    function GetList(out result_state: boolean; out ErrorMessage: string): TDataSet;
    procedure FillDictionary(var dest: TList<TPair<Variant, string>>; context: TJSONObject; keyname, displayname: string; out result_state: boolean; out ErrorMessage: string);
    function Insert(data: TDataSet; out ErrorMessage: string): boolean;
    function Delete(data: TDataSet; out ErrorMessage: string): boolean;
    function Update(data: TDataSet; out ErrorMessage: string): boolean;
    property AvoidEvents: boolean read GetAvoidEvents write SetAvoidEvents;
  published
    property OnBeforeGetList: TNotifyEvent read FOnBeforeGetList write FOnBeforeGetList;

    property OnBeforeInsert: TFMADataSetDelegate read FOnBeforeInsert write FOnBeforeInsert;
    property OnBeforeEdit: TFMADataSetDelegate read FOnBeforeEdit write FOnBeforeEdit;
    property OnBeforeDelete: TFMADataSetDelegate read FOnBeforeDelete write FOnBeforeDelete;

    property InsertExecutor: TFMADataSetDelegate read FInsertExecutor write FInsertExecutor;
    property EditExecutor: TFMADataSetDelegate read FEditExecutor write FEditExecutor;
    property DeleteExecutor: TFMADataSetDelegate read FDeleteExecutor write FDeleteExecutor;

    property OnAfterInsert: TFMADataSetDelegate read FOnAfterInsert write FOnAfterInsert;
    property OnAfterEdit: TFMADataSetDelegate read FOnAfterEdit write FOnAfterEdit;
    property OnAfterDelete: TFMADataSetDelegate read FOnAfterDelete write FOnAfterDelete;

    property ServerMethodGetList: TSqlServerMethod read FServerMethodGetList write FServerMethodGetList;
    property ParamTransform: TFMAParamTransform read GetParamTransform;

    property Active: boolean read FActive write SetActive;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Project Controls', [TFMACrudAgent]);
end;

{ TFMACrudAgent }

constructor TFMACrudAgent.Create(AOwner: TComponent);
begin
  inherited;
  FParamTransform := TFMAParamTransform.Create(self);
  FBuffer := TStringStream.Create;
  AvoidEvents := false;
end;

function TFMACrudAgent.Insert(data: TDataSet; out ErrorMessage: string): boolean;
begin
  result := false;
  if Assigned(FInsertExecutor) then
  begin
    FInsertExecutor(data, result, ErrorMessage);
    if not result then
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

function TFMACrudAgent.Delete(data: TDataSet; out ErrorMessage: string): boolean;
begin
  result := false;
  if Assigned(FDeleteExecutor) then
  begin
    FDeleteExecutor(data, result, ErrorMessage);
    if not result then
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

function TFMACrudAgent.Update(data: TDataSet; out ErrorMessage: string): boolean;
begin
  result := false;
  if Assigned(FEditExecutor) then
  begin
    FEditExecutor(data, result, ErrorMessage);
    if not result then
    MessageDlg(errormessage, mtError, [mbOK], 0);
  end;
end;

procedure TFMACrudAgent.FillDictionary(var dest: TList<TPair<Variant, string>>; context: TJSONObject; keyname, displayname: string; out result_state: boolean; out ErrorMessage: string);
var ds: TDataSet; p: TPair<Variant, string>;
begin
  dest.Clear;
  ds := GetList(result_state, ErrorMessage);
  ds.First;
  while not ds.Eof do
  begin
    p.Key := ds.FieldByName(keyname).Value;
    if ds.FieldByName(displayname).IsBlob then
    begin
      TBlobField(ds.FieldByName(displayname)).SaveToStream(FBuffer);
      p.Value := StreamToString(FBuffer);
    end else
    begin
      p.Value := trim(ds.FieldByName(displayname).AsString);
    end;
    dest.Add(p);
    ds.Next;
  end;
end;

function TFMACrudAgent.GetAvoidEvents: boolean;
begin
  result := FAvoidEvents;
end;

function TFMACrudAgent.GetList(out result_state: boolean; out ErrorMessage: string): TDataSet;
var p: TParam;
begin
  result := nil;
  if not AvoidEvents then
  begin
    if Assigned(FOnBeforeGetList) then
    OnBeforeGetList(Owner);
  end;
  if Assigned(FServerMethodGetList) then
  begin
    FServerMethodGetList.Close;
    p := FServerMethodGetList.Params.FindParam('filter');
    if Assigned(p) then
    begin
      if not Assigned(FParamTransform.Context) then
      FParamTransform.ResetContext;
      p.Clear;
      p.SetObjectValue(FParamTransform.Context, ftObject, false);
    end;
    try
      FServerMethodGetList.Open;
    except
      on E: Exception do
      begin
        ErrorMessage := E.Message;
        result_state := false;
        result := nil;
        exit;
      end;
    end;
    result := FServerMethodGetList;
  end;
end;

function TFMACrudAgent.GetParamTransform: TFMAParamTransform;
begin
  result := FParamTransform;
end;

procedure TFMACrudAgent.SetActive(value: boolean);
var result_state: boolean; errormessage: string;
begin
  FActive := value;
  if value then GetList(result_state, errormessage);
end;

procedure TFMACrudAgent.SetAvoidEvents(value: boolean);
begin
  FAvoidEvents := value;
end;

end.

