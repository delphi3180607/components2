unit FMACrudAgentInterface;

interface

uses
  System.SysUtils, System.Classes, JSON, DB, Generics.Collections, FMAParamTransform;

type
  IFMACrudAgent = interface
    function GetParamTransform: TFMAParamTransform;
    function GetAvoidEvents: boolean;
    procedure SetAvoidEvents(value: boolean);
    function GetList(out result_state: boolean; out ErrorMessage: string): TDataSet;
    procedure FillDictionary(var dest: TList<TPair<Variant, string>>; context: TJSONObject; keyname, displayname: string; out result_state: boolean; out ErrorMessage: string); overload;
    function Insert(data: TDataSet; out ErrorMessage: string): boolean;
    function Delete(data: TDataSet; out ErrorMessage: string): boolean;
    function Update(data: TDataSet; out ErrorMessage: string): boolean;
    property ParamTransform: TFMAParamTransform read GetParamTransform;
    property AvoidEvents: boolean read GetAvoidEvents write SetAvoidEvents;
  end;

implementation


end.
