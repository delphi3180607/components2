unit filterclass;

interface

uses SysUtils, System.Generics.Defaults, System.Generics.Collections;

type TFilterOperation = (boAnd, boOr, coEqual, coMore, coLess, coMoreOrEqual, coLessOrEqual, coLike, coLikeCaseSensetive, foNone);

type TFilterParameter = record
  Value: Variant;
  Name: string;
end;

type TFilterParameters = TList<TFilterParameter>;

type TFilterCustomExpression = class(TObject)
  private
    function GenerateParameterName: string;
  public
    Parameters: TFilterParameters;
    RootExpression: TFilterCustomExpression;
    constructor Create; overload; virtual;
    function BuildSql(rootExp: TFilterCustomExpression): string; virtual;
    function DecodeOperation(operation: TFilterOperation): string;
end;

type TFilterCustomOperand = class(TFilterCustomExpression);

type TFilterOperandField = class(TFilterCustomOperand)
public
  FieldName: string;
  function BuildSql(rootExp: TFilterCustomExpression): string; override;
end;

type TFilterOperandValue = class(TFilterCustomOperand)
public
  Value: Variant;
  function BuildSql(rootExp: TFilterCustomExpression): string; override;
end;

type TFilterBinaryExpression = class(TFilterCustomExpression)
public
  Operand1: TFilterCustomExpression;
  Operand2: TFilterCustomExpression;
  Operation: TFilterOperation;
  function BuildSql(rootExp: TFilterCustomExpression): string; override;
end;

type TFilterSerialExpression = class(TFilterCustomExpression)
public
  Operands: TList<TFilterCustomExpression>;
  Operation: TFilterOperation;
  function BuildSql(rootExp: TFilterCustomExpression): string; override;
  constructor Create; override;
end;

type TFilterFactory = class
  public
  class function CreateBinaryExpression(operand1, operand2: TFilterCustomOperand; operation: TFilterOperation): TFilterBinaryExpression; overload; static;
  class function CreateBinaryExpression(fieldName: string; value: Variant; operation: TFilterOperation): TFilterBinaryExpression; overload; static;
  class function CreateBinaryExpression(fieldName1, fieldName2: string; operation: TFilterOperation): TFilterBinaryExpression; overload; static;
  class function CreateOperandField(fieldName: string): TFilterOperandField; static;
  class function CreateOperandValue(value: Variant): TFilterOperandValue; static;
end;

implementation

constructor TFilterCustomExpression.Create;
begin
  inherited Create;
  parameters := TFilterParameters.Create;
end;

function TFilterCustomExpression.DecodeOperation(operation: TFilterOperation): string;
begin
  result := '?';
  case operation of
    boAnd: result := ' and ';
    boOr: result := ' or ';
    coEqual: result := ' = ';
    coMore: result := ' > ';
    coLess: result := ' < ';
    coMoreOrEqual: result := ' >= ';
    coLessOrEqual: result := ' <= ';
    coLike: result := ' like ';
    coLikeCaseSensetive: result := ' like ';
    foNone: result := '';
  end;
end;

function TFilterCustomExpression.GenerateParameterName: string;
var parameterName: string;
begin
   parameterName := parameterName+trim(IntToStr(Parameters.Count+1));
   result := ' :p'+parameterName;
end;

function TFilterCustomExpression.BuildSql(rootExp: TFilterCustomExpression): string;
begin
  if Assigned(rootExp) then
    RootExpression := rootExp
  else
    RootExpression := self;
  result := '';
end;

function TFilterOperandField.BuildSql(rootExp: TFilterCustomExpression): string;
var sql: string;
begin
  inherited BuildSql(rootExp);
  sql := '';
  sql := FieldName;
  result := sql;
end;

function TFilterOperandValue.BuildSql(rootExp: TFilterCustomExpression): string;
var p: TFilterParameter;
begin
  inherited BuildSql(rootExp);
  p.Name := RootExpression.GenerateParameterName;
  p.Value := Value;
  RootExpression.Parameters.Add(p);
  result := p.Name;
end;

function TFilterBinaryExpression.BuildSql(rootExp: TFilterCustomExpression): string;
var sql: string;
begin
  inherited BuildSql(rootExp);
  sql := '';
  sql := Operand1.BuildSql(RootExpression)+DecodeOperation(Operation)+Operand2.BuildSql(RootExpression);
  result := '('+sql+')';
end;

constructor TFilterSerialExpression.Create;
begin
  inherited Create;
  Operands := TList<TFilterCustomExpression>.Create;
end;

function TFilterSerialExpression.BuildSql(rootExp: TFilterCustomExpression): string;
var e: TFilterCustomExpression; sql, sOperation, sOper: string;
begin
  inherited BuildSql(rootExp);
  sOperation := self.DecodeOperation(Operation);
  sOper := '';
  for e in Operands do
  begin
    sql := sql+sOper+e.BuildSql(RootExpression);
    sOper := sOperation;
  end;
  result := '('+sql+')';
end;

class function TFilterFactory.CreateBinaryExpression(operand1, operand2: TFilterCustomOperand; operation: TFilterOperation): TFilterBinaryExpression;
begin
  result := TFilterBinaryExpression.Create;
  result.Operand1 := operand1;
  result.Operand2 := operand2;
  result.Operation := operation;
end;

class function TFilterFactory.CreateBinaryExpression(fieldName: string; value: Variant; operation: TFilterOperation): TFilterBinaryExpression;
begin
  result := TFilterBinaryExpression.Create;
  result.Operand1 := CreateOperandField(fieldName);
  result.Operand2 := CreateOperandValue(value);
  result.Operation := operation;
end;

class function TFilterFactory.CreateBinaryExpression(fieldName1, fieldName2: string; operation: TFilterOperation): TFilterBinaryExpression;
begin
  result := TFilterBinaryExpression.Create;
  result.Operand1 := CreateOperandField(fieldName1);
  result.Operand1 := CreateOperandField(fieldName2);
  result.Operation := operation;
end;

class function TFilterFactory.CreateOperandField(fieldName: string): TFilterOperandField;
begin
  result := TFilterOperandField.Create;
  result.FieldName := fieldName;
end;

class function TFilterFactory.CreateOperandValue(value: Variant): TFilterOperandValue;
begin
  result := TFilterOperandValue.Create;
  result.Value := value;
end;

end.
